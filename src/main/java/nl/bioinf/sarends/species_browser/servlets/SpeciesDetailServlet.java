package nl.bioinf.sarends.species_browser.servlets;

import nl.bioinf.sarends.species_browser.config.WebConfig;
import nl.bioinf.sarends.species_browser.model.HistoryManager;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "SpeciesDetailServlet", urlPatterns = "/species.detail", loadOnStartup = 1)
public class SpeciesDetailServlet extends HttpServlet {

    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[PhraseServlet] Running no-arg init()-detail");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[PhraseServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[PhraseServlet] Shutting down servlet service");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());


//        final ServletContext servletContext = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process("listing", ctx, response.getWriter());
        templateEngine.process("species_detail",ctx, response.getWriter());
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String animalString = request.getParameter("animal");
        String[] animalObjects = animalString.split(";");
        String scientificName = animalObjects[0];
        String englishName = animalObjects[1];
        String dutchName = animalObjects[2];
        String wingSpan = animalObjects[3];
        String pictureLocation = animalObjects[4];
        String name = animalObjects[5].toLowerCase();

        HttpSession session = request.getSession();

        HistoryManager history = (HistoryManager) session.getAttribute("history");

        if (session.getAttribute("user") == null){
            response.sendRedirect("/login");
        }

        if (history != null){
            history.addItem(englishName);
            session.setAttribute("history", history);
        } else{
            HistoryManager historyManager = new HistoryManager();
            historyManager.addItem(englishName);
            session.setAttribute("history", historyManager);
        }

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());


        ctx.setVariable("currentDate", new Date());
        ctx.setVariable("scienceName", scientificName);
        ctx.setVariable("engName", englishName);
        ctx.setVariable("nlName", dutchName);
        ctx.setVariable("wingSpan", wingSpan);
        ctx.setVariable("picLocation", pictureLocation);
        ctx.setVariable("name", name);
        ctx.setVariable("history", history);

        templateEngine.process("species_detail",ctx, response.getWriter());

//        final ServletContext servletContext = super.getServletContext();
//
//        WebConfig.createTemplateEngine(servletContext).process("species_detail", ctx, response.getWriter());

    }
}
