package nl.bioinf.sarends.species_browser.servlets;

import nl.bioinf.sarends.species_browser.config.WebConfig;
import nl.bioinf.sarends.species_browser.model.Animal;
import nl.bioinf.sarends.species_browser.model.HistoryManager;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet(name = "SpeciesServlet", urlPatterns = "/home", loadOnStartup = 1)
public class SpeciesServlet extends HttpServlet {

    private String file;
    static List<Animal> animalList;
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[PhraseServlet] Running no-arg init()-home");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        animalList = WebConfig.getAnimalList();

    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[PhraseServlet] Shutting down servlet service");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

//        final ServletContext servletContext = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process("listing", ctx, response.getWriter());

        templateEngine.process("listing",ctx, response.getWriter());
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        //CsvParser csvParser = new CsvParser();
        //List<Animals> animalList = csvParser.createAnimalList(csvParser.parseFile("/homes/sarends/jaar_3/Thema_10/WIS/speciesbrowser/data/species.csv"));
        //List<Animals> animalList = csvParser.createAnimalList(csvParser.parseFile(getServletContext().getInitParameter("species_file_home")));

        HttpSession session = request.getSession();

        HistoryManager history = (HistoryManager) session.getAttribute("history");

        if (session.getAttribute("user") == null){
            response.sendRedirect("/login");
        }

        if (history != null){
            history.addItem("Home");
            session.setAttribute("history", history);
        } else{
            HistoryManager historyManager = new HistoryManager();
            historyManager.addItem("Home");
            session.setAttribute("history", historyManager);
        }


        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("currentDate", new Date());
        ctx.setVariable("animalsList", animalList);
        ctx.setVariable("history", history);


//        final ServletContext servletContext = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process("listing", ctx, response.getWriter());

        templateEngine.process("listing",ctx, response.getWriter());
    }
}
