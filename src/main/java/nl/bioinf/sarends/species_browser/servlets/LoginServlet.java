package nl.bioinf.sarends.species_browser.servlets;

import nl.bioinf.sarends.species_browser.config.WebConfig;
import nl.bioinf.sarends.species_browser.dao.DataBaseException;
import nl.bioinf.sarends.species_browser.dao.UserDaoFactory;
import nl.bioinf.sarends.species_browser.dao.UserInterface;
import nl.bioinf.sarends.species_browser.model.HistoryManager;
import nl.bioinf.sarends.species_browser.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login", loadOnStartup = 1)
public class LoginServlet extends HttpServlet {

    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[PhraseServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[PhraseServlet] Running init(ServletConfig config)-login");
//        config.getServletContext().getInitParameter("admin_email");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[PhraseServlet] Shutting down servlet service");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = request.getParameter("username");
        String passWord = request.getParameter("password");
        Object logout = request.getParameter("logout");

        if(logout!=null){
            session.removeAttribute("user");
            response.sendRedirect("/login");
        }

        HistoryManager history = (HistoryManager) session.getAttribute("history");


        if (history != null){
            history.addItem("Login");
            session.setAttribute("history", history);
        } else{
            HistoryManager historyManager = new HistoryManager();
            historyManager.addItem("Login");
            session.setAttribute("history", historyManager);
        }


        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("username", userName);


        String nextPage = "";

        final UserInterface dataSource = UserDaoFactory.getDataSource();

        if (session.isNew() || session.getAttribute("user") == null){
            try {
                final User user = dataSource.getUser(userName, passWord);
                if (user != null){
                    session.setAttribute("user", user);
                    nextPage = "listing";
                    ctx.setVariable("animalsList", WebConfig.getAnimalList());

                } else{
                    ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                    ctx.setVariable("message_type", "error");
                    nextPage = "login";
                }

            } catch (DataBaseException e) {
                e.printStackTrace();
            }
        } else{
            nextPage = "listing";
            //response.sendRedirect("/home");
            ctx.setVariable("animalsList", WebConfig.getAnimalList());
        }

//        if (session.isNew() || session.getAttribute("user") == null){
//            Boolean authenticated = authenticate(userName, passWord);
//            if (authenticated){
//                session.setAttribute("user", new User(userName, passWord,User.Role.ADMIN));
//                nextPage = "listing";
//                //https://www.javatpoint.com/sendRedirect()-method
//                ctx.setVariable("animalsList", WebConfig.getAnimalList());
//                //response.sendRedirect("/home");
//            }else{
//                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
//                ctx.setVariable("message_type", "error");
//                nextPage = "login";
//            }
//        } else{
//            nextPage = "listing";
//            //response.sendRedirect("/home");
//            ctx.setVariable("animalsList", WebConfig.getAnimalList());
//        }

//
//        final ServletContext servletContext = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process(nextPage, ctx, response.getWriter());

        templateEngine.process(nextPage,ctx, response.getWriter());

    }

    private boolean authenticate(String username, String password) {
        return username.equals("Stijn") && password.equals("wachtwoord");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

//        final ServletContext servletContext = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process("login", ctx, response.getWriter());
       templateEngine.process("login",ctx, response.getWriter());

    }
}
