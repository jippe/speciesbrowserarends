package nl.bioinf.sarends.species_browser.dao;

import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;
import nl.bioinf.sarends.species_browser.model.User;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.IdentityHashMap;

public class MyDbConnector implements UserInterface {
    private final String url;
    private final String dbUser;
    private final String dbPassword;
    private Connection connection;
    private HashMap<String, PreparedStatement> preparedStatements = new HashMap<>();
    private static final String GET_USER = "get_user";
    private static final String INSERT_USER = "insert_user";

//    public static void main(String[] args) throws DataBaseException {
//        MyDbConnector myDbConnector = new MyDbConnector();
//        myDbConnector.connect();
//    }

    /*singleton pattern*/
    private static MyDbConnector uniqueInstance;

    /**
     * singleton pattern
     * @param url
     * @param dbUser
     * @param dbPassword
     */
    private MyDbConnector(String url, String dbUser, String dbPassword) {
        this.url = url;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    /**
     * singleton pattern
     */
    public static MyDbConnector getInstance(String url, String dbUser, String dbPassword) {
        //lazy
        if (uniqueInstance == null) {
            uniqueInstance = new MyDbConnector(url, dbUser, dbPassword);
        }
        return uniqueInstance;
    }

    @Override
    public void connect() throws DataBaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            prepareStatements();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataBaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }


    }

    @Override
    public void disconnect() throws DataBaseException {
        try{
            for( String key : this.preparedStatements.keySet() ){
                this.preparedStatements.get(key).close();
            }
        }catch( Exception e ){
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User getUser(String userName, String userPass) throws DataBaseException {
        PreparedStatement preparedStatement = this.preparedStatements.get(GET_USER);
        try {
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, userPass);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.getFetchSize() > 1) {
                throw new DataBaseException("something wrong");
            }

            while(resultSet.next()){
                String userIdStr = resultSet.getString("user_id");
                String userRoleStr = resultSet.getString("user_role");
                User.Role userRole = User.Role.valueOf(userRoleStr);
                User user = new User(userName, userPass, userRole);
                return user;
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public void insertUser(String userName, String userPass, User.Role role) throws DataBaseException {
        PreparedStatement preparedStatement = this.preparedStatements.get(INSERT_USER);
        try {
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, userPass);
            preparedStatement.setString(3, role.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void prepareStatements() throws SQLException {
        String fetchQuery = "SELECT * FROM Users WHERE user_name = ? AND user_password = ?";
        PreparedStatement ps = connection.prepareStatement(fetchQuery);
        this.preparedStatements.put(GET_USER, ps);

        String insertQuery = "INSERT INTO Users (user_name, user_password, user_email, user_role) "
                + " VALUES (?, ?, ?, ?)";
        ps = connection.prepareStatement(insertQuery);
        this.preparedStatements.put(INSERT_USER, ps);
    }

}
