package nl.bioinf.sarends.species_browser.dao;

import nl.bioinf.sarends.species_browser.model.User;


public interface UserInterface {

    void connect() throws DataBaseException;

    void disconnect() throws DataBaseException;

    User getUser(String userName, String userPass) throws DataBaseException;

    void insertUser(String userName, String userPass, User.Role role) throws DataBaseException;


}
