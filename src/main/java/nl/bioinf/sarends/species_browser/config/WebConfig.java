package nl.bioinf.sarends.species_browser.config;

import nl.bioinf.sarends.species_browser.dao.DataBaseException;
import nl.bioinf.sarends.species_browser.dao.UserDaoFactory;
import nl.bioinf.sarends.species_browser.model.Animal;
import nl.bioinf.sarends.species_browser.model.CsvParser;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.FileNotFoundException;
import java.util.List;


@WebListener
public class WebConfig implements ServletContextListener {
    private static TemplateEngine templateEngine;
    private String file;
    private static List<Animal> animalList;


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("[WebConfig] Initializing template engine");
        createTemplateEngine(servletContextEvent.getServletContext());

        String dbType = servletContextEvent.getServletContext().getInitParameter("db-type");
        try {
            UserDaoFactory.initializeDataSource(dbType);
        } catch (DataBaseException e) {
            e.printStackTrace();
        }

        file = servletContextEvent.getServletContext().getInitParameter("species_file");
        //file = config.getServletContext().getInitParameter("species_file_home");

        CsvParser csvParser = new CsvParser();
        try {
            animalList = csvParser.createAnimalList(csvParser.parseFile(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Shutting down!");
    }

    private static void createTemplateEngine(ServletContext servletContext) {
        ServletContextTemplateResolver templateResolver =
                new ServletContextTemplateResolver(servletContext);
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheTTLMs(3600000L);
        // Cache is set to true by default.
        // Set to false if you want templates to be automatically
        // updated when modified.
        templateResolver.setCacheable(true);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        WebConfig.templateEngine = templateEngine;
    }


    public static List<Animal> getAnimalList() {
        return animalList;
    }

    /**
     * serves the template engine that was created at application startup.
     * @return
     */
    public static TemplateEngine getTemplateEngine() {
        return templateEngine;
    }

    //method omitted
}